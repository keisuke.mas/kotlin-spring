package com.example.kotlinspring.controller

import org.springframework.web.bind.annotation.RequestMapping
import org.springframework.web.bind.annotation.RestController

@RestController

class ApiController {

    @RequestMapping("/")
    fun rootPath(): String? {
        return "こんにちはこんにちは"
    }
}